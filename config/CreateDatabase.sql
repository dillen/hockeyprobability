BEGIN;

CREATE DATABASE IF NOT EXISTS `hockey_probability` DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci;
USE `hockey_probability`;

DROP TABLE IF EXISTS `abbreviations`;

CREATE TABLE `abbreviations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(255) DEFAULT NULL,
  `team_abbrev` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `schedule`;

CREATE TABLE `schedule` (
  `date` varchar(255) DEFAULT NULL,
  `visitor` varchar(255) DEFAULT NULL,
  `home` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1231 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `schedule_20162017`;

CREATE TABLE `schedule_20162017` (
  `id` int(11) NOT NULL,
  `game_id` int(11) DEFAULT NULL,
  `season` int(11) DEFAULT NULL,
  `session` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_time` varchar(255) DEFAULT NULL,
  `home_team` varchar(255) DEFAULT NULL,
  `home_division` varchar(255) DEFAULT NULL,
  `home_conference` varchar(255) DEFAULT NULL,
  `home_score` int(11) DEFAULT NULL,
  `away_team` varchar(255) DEFAULT NULL,
  `away_division` varchar(255) DEFAULT NULL,
  `away_conference` varchar(255) DEFAULT NULL,
  `away_score` int(11) DEFAULT NULL,
  `timezone` varchar(255) DEFAULT NULL,
  `home_shots` int(11) DEFAULT NULL,
  `away_shots` int(11) DEFAULT NULL,
  `home_goals` int(11) DEFAULT NULL,
  `away_goals` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `team`;

CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expected_goals_for` double NOT NULL,
  `actual_goals_for` double NOT NULL,
  `last_game_goals_for` double NOT NULL,
  `result_XGF` double NOT NULL,
  `result_AGF` double NOT NULL,
  `result_last_GF` double NOT NULL,
  `team_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `team_stats`;

CREATE TABLE `team_stats` (
  `id` int(11) NOT NULL,
  `Team` varchar(255) DEFAULT NULL,
  `Season` varchar(255) DEFAULT NULL,
  `SeasonType` varchar(255) DEFAULT NULL,
  `GP` int(11) DEFAULT NULL,
  `TOI` double DEFAULT NULL,
  `CF` int(11) DEFAULT NULL,
  `CA` int(11) DEFAULT NULL,
  `CF60` double DEFAULT NULL,
  `CA60` double DEFAULT NULL,
  `CF%` double DEFAULT NULL,
  `CSh%` double DEFAULT NULL,
  `CSv%` double DEFAULT NULL,
  `FF` int(11) DEFAULT NULL,
  `FA` int(11) DEFAULT NULL,
  `FF60` double DEFAULT NULL,
  `FA60` double DEFAULT NULL,
  `FF%` double DEFAULT NULL,
  `FSh%` double DEFAULT NULL,
  `FSv%` double DEFAULT NULL,
  `SF` int(11) DEFAULT NULL,
  `SA` int(11) DEFAULT NULL,
  `SF60` double DEFAULT NULL,
  `SA60` double DEFAULT NULL,
  `SF%` double DEFAULT NULL,
  `Sh%` double DEFAULT NULL,
  `Sv%` double DEFAULT NULL,
  `xGF` double DEFAULT NULL,
  `xGA` double DEFAULT NULL,
  `xGF60` double DEFAULT NULL,
  `xGA60` double DEFAULT NULL,
  `xGF%` double DEFAULT NULL,
  `SCF` int(11) DEFAULT NULL,
  `SCA` int(11) DEFAULT NULL,
  `SCF60` double DEFAULT NULL,
  `SCA60` double DEFAULT NULL,
  `SCF%` double DEFAULT NULL,
  `xFSh%` double DEFAULT NULL,
  `xFSv%` double DEFAULT NULL,
  `AdjFSv%` double DEFAULT NULL,
  `PDO` double DEFAULT NULL,
  `xPDO` double DEFAULT NULL,
  `GF` double DEFAULT NULL,
  `GA` double DEFAULT NULL,
  `GF60` double DEFAULT NULL,
  `GA60` double DEFAULT NULL,
  `GF%` double DEFAULT NULL,
  `FO%` double DEFAULT NULL,
  `PENDIFF` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELIMITER $$  
CREATE PROCEDURE init_abbreviations()

BEGIN
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Florida Panthers", "FLA");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("New York Islanders", "NYI");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Buffalo Sabres", "BUF");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Tampa Bay Lightning", "T.B");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Colorado Avalanche", "COL");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("New York Rangers", "NYR");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Toronto Maple Leafs", "TOR");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Detroit Red Wings", "DET");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Calgary Flames", "CGY");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Dallas Stars", "DAL");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Washington Capitals", "WSH");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Winnipeg Jets", "WPG");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Nashville Predators", "NSH");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("St. Louis Blues", "STL");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("New Jersey Devils", "N.J");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Chicago Blackhawks", "CHI");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Edmonton Oilers", "EDM");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Arizona Coyotes", "ARI");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Ottawa Senators", "OTT");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Montreal Canadiens", "MTL");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Minnesota Wild", "MIN");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Columbus Blue Jackets", "CBJ");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Vancouver Canucks", "VAN");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Boston Bruins", "BOS");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("San Jose Sharks", "S.J");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Los Angeles Kings", "L.A");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Carolina Hurricanes", "CAR");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Anaheim Ducks", "ANA");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Philadelphia Flyers", "PHI");
		INSERT INTO `abbreviations` (team_name, team_abbrev) VALUES("Pittsburgh Penguins", "PIT");

END $$
DELIMITER ;


CALL `init_movie`;
DROP PROCEDURE `init_movie`;

commit;