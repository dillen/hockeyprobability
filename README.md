# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This is a student project for calculating win probabilty in hockey. Specifically for the NHL regualar season. 


### How do I get set up? ###
To install and run this application you need Wildfly 10. 


** Configuration **

Download and install Wildfly 10.

You will need mysql and a mysql connector, download here: https://dev.mysql.com/downloads/connector/j/ 

Using a terminal cd into the bin folder in the wildfly folder. 

Run the add-user.sh(add-user.bat on windows) and follow the instructions to create a user.

In the same folder,./wildfly/bin, run standalone.sh(standalone.bat on windows) to start a instance of the wildfly server.

Then run jboss-cli.sh(jboss-cli.bat) and follow the instructions specified in the ConfigWildfly.cli file in the folder HockeyProbability/config

** Database configuration **
Mysql

Run the sql scripts in HockeyProbability/config/

First run the CreateUser.sql to create myslq user

The run the CreateDatabase.sql to create the database and tables.