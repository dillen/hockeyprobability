package dillner.josef.hockeyprobability.utils;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class ArchiveBase {

	public static Archive<WebArchive> createTestArchive(String warName) {
		return ShrinkWrap.create(WebArchive.class, warName+".war")
				.addPackages(true, "dillner.josef.hockeyprobability.utils")
				.addPackages(true, "dillner.josef.hockeyprobability.util")
				.addPackages(true, "dillner.josef.hockeyprobability.entities")
				.addPackages(true, "dillner.josef.hockeyprobability.calculations")
				.addPackages(true, "dillner.josef.hockeyprobability.domainobject")
				.addPackages(true, "dillner.josef.hockeyprobability.logic")
				.addPackages(true, "dillner.josef.hockeyprobability.gui")
				.addPackages(true, "dillner.josef.hockeyprobability.persistence")
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource("test-ds.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}
}
