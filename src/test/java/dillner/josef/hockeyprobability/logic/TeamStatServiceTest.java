package dillner.josef.hockeyprobability.logic;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import static org.junit.Assert.assertEquals;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import dillner.josef.hockeyprobability.domainobject.GameDO;
import dillner.josef.hockeyprobability.util.TwitterUtil;
import dillner.josef.hockeyprobability.utils.ArchiveBase;

@RunWith(Arquillian.class)
public class TeamStatServiceTest {

	
	@Deployment
	public static Archive<WebArchive> createTestArchive() {
		final WebArchive archive = (WebArchive)ArchiveBase.createTestArchive("team-stat-service-test");
		archive.deleteClass(TwitterUtil.class);
		return archive;
	}
	
	@Inject
	private TeamStatService teamStatService;
	
	@Test
	public void testInject() {
		assertNotNull(this.teamStatService);
	}
	
	@Test
	public void testCalculateTeamProbability() throws Exception {
		final List<GameDO> todaysGames = this.teamStatService.calculateTeamProbability();
		for (int i = 0; i < todaysGames.size(); i++) {
			System.out.println("TodaysGames: " + todaysGames.get(i).getHomeTeam());
		}
	}
}
