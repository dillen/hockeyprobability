package dillner.josef.hockeyprobability.presentation.gui;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import dillner.josef.hockeyprobability.domainobject.GameDO;
import dillner.josef.hockeyprobability.logic.TeamStatService;
import dillner.josef.hockeyprobability.logic.UpdateDatabaseService;
import dillner.josef.hockeyprobability.logic.exception.PersistenceOperationException;

@Dependent
public class TeamStatBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private TeamStatService teamStatService;
	
	@Inject
	private UpdateDatabaseService updateDatabaseService;
	
	@Inject
	private Logger logger;

	private List<GameDO> games;

	public List<GameDO> getGames() {
		return games;
	}

	public void setGames(List<GameDO> games) {
		this.games = games;
	}

	@PostConstruct
	public void start() {
		try {
			String result = this.updateDatabaseService.updateDatabase();
			System.out.println(result);
			this.games = this.teamStatService.calculateTeamProbability();
		} catch (PersistenceOperationException e) {
			this.logger.severe("Failed to calculate games: " + e.getMessage());
		} catch (Exception e) {
			this.logger.severe("Failed to update games: " + e.getMessage());
		}
	}

}
