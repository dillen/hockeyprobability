package dillner.josef.hockeyprobability.presentation.gui;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import dillner.josef.hockeyprobability.domainobject.GameDO;

@Named
@SessionScoped
public class TeamStatController implements Serializable{
	private static final long serialVersionUID = 1L;

	@Inject 
	private TeamStatBean teamStatBean;
	
	public List<GameDO> getGames() {
		return this.teamStatBean.getGames();
	}
	
	public void start(){
		this.teamStatBean.start();
	}
}
