package dillner.josef.hockeyprobability.presentation.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import dillner.josef.hockeyprobability.domainobject.GameDO;
import dillner.josef.hockeyprobability.logic.TeamStatService;

@RequestScoped
@Path("/games")
@Produces("application/json")
@Consumes("application/json")
public class GamesRestService {

	@Inject
	private TeamStatService teamStatService;
	
	@GET
	public List<GameDO> getGames() throws Exception{
		final List<GameDO> games = teamStatService.calculateTeamProbability();
		return games;
	}
}
