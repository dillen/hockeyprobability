package dillner.josef.hockeyprobability.domainobject;

import dillner.josef.hockeyprobability.entities.Team;

public class GameDO {

	private Team homeTeam;
	private Team awayTeam;

	private String homeTeamName;
	private String awayTeamName;

	private double homeTeamChance;
	private double awayTeamChance;

	public GameDO() {
	}

	public Team getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(Team homeTeam) {
		this.homeTeam = homeTeam;
	}

	public Team getAwayTeam() {
		return awayTeam;
	}

	public void setAwayTeam(Team awayTeam) {
		this.awayTeam = awayTeam;
	}

	public double getHomeTeamChance() {
		return homeTeamChance;
	}

	public void setHomeTeamChance(double homeTeamChance) {
		this.homeTeamChance = homeTeamChance;
	}

	public double getAwayTeamChance() {
		return awayTeamChance;
	}

	public void setAwayTeamChance(double awayTeamChance) {
		this.awayTeamChance = awayTeamChance;
	}

	public String getHomeTeamName() {
		return homeTeamName;
	}

	public void setHomeTeamName(String homeTeamName) {
		this.homeTeamName = homeTeamName;
	}

	public String getAwayTeamName() {
		return awayTeamName;
	}

	public void setAwayTeamName(String awayTeamName) {
		this.awayTeamName = awayTeamName;
	}

}
