package dillner.josef.hockeyprobability.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the schedule_20162017 database table.
 * 
 */
@Entity
@Table(name="schedule_20162017")
@NamedQueries({
	@NamedQuery(name="Schedule20162017.findAll", query="SELECT s FROM Schedule20162017 s"),
	@NamedQuery(name="Schedule20162017.findByDate", query="SELECT s FROM Schedule20162017 s WHERE s.dateTime LIKE :dateTime OR s.dateTime LIKE :inputWithSpace")
})
public class Schedule20162017 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="away_conference")
	private String awayConference;

	@Column(name="away_division")
	private String awayDivision;

	@Column(name="away_goals")
	private int awayGoals;

	@Column(name="away_score")
	private int awayScore;

	@Column(name="away_shots")
	private int awayShots;

	@Column(name="away_team")
	private String awayTeam;

	@Column(name="date_time")
	private String dateTime;

	@Column(name="game_id")
	private int gameId;

	@Column(name="home_conference")
	private String homeConference;

	@Column(name="home_division")
	private String homeDivision;

	@Column(name="home_goals")
	private int homeGoals;

	@Column(name="home_score")
	private int homeScore;

	@Column(name="home_shots")
	private int homeShots;

	@Column(name="home_team")
	private String homeTeam;

	private int season;

	private String session;

	private String status;

	private String timezone;

	public Schedule20162017() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAwayConference() {
		return this.awayConference;
	}

	public void setAwayConference(String awayConference) {
		this.awayConference = awayConference;
	}

	public String getAwayDivision() {
		return this.awayDivision;
	}

	public void setAwayDivision(String awayDivision) {
		this.awayDivision = awayDivision;
	}

	public int getAwayGoals() {
		return this.awayGoals;
	}

	public void setAwayGoals(int awayGoals) {
		this.awayGoals = awayGoals;
	}

	public int getAwayScore() {
		return this.awayScore;
	}

	public void setAwayScore(int awayScore) {
		this.awayScore = awayScore;
	}

	public int getAwayShots() {
		return this.awayShots;
	}

	public void setAwayShots(int awayShots) {
		this.awayShots = awayShots;
	}

	public String getAwayTeam() {
		return this.awayTeam;
	}

	public void setAwayTeam(String awayTeam) {
		this.awayTeam = awayTeam;
	}

	public String getDateTime() {
		return this.dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public int getGameId() {
		return this.gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public String getHomeConference() {
		return this.homeConference;
	}

	public void setHomeConference(String homeConference) {
		this.homeConference = homeConference;
	}

	public String getHomeDivision() {
		return this.homeDivision;
	}

	public void setHomeDivision(String homeDivision) {
		this.homeDivision = homeDivision;
	}

	public int getHomeGoals() {
		return this.homeGoals;
	}

	public void setHomeGoals(int homeGoals) {
		this.homeGoals = homeGoals;
	}

	public int getHomeScore() {
		return this.homeScore;
	}

	public void setHomeScore(int homeScore) {
		this.homeScore = homeScore;
	}

	public int getHomeShots() {
		return this.homeShots;
	}

	public void setHomeShots(int homeShots) {
		this.homeShots = homeShots;
	}

	public String getHomeTeam() {
		return this.homeTeam;
	}

	public void setHomeTeam(String homeTeam) {
		this.homeTeam = homeTeam;
	}

	public int getSeason() {
		return this.season;
	}

	public void setSeason(int season) {
		this.season = season;
	}

	public String getSession() {
		return this.session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTimezone() {
		return this.timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

}