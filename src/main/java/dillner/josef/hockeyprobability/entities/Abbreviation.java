package dillner.josef.hockeyprobability.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the abbreviations database table.
 * 
 */
@Entity
@Table(name="abbreviations")
@NamedQueries({
	@NamedQuery(name="Abbreviation.findAll", query="SELECT a FROM Abbreviation a"),
	@NamedQuery(name="Abbreviation.findByName", query="SELECT a FROM Abbreviation a WHERE a.teamName LIKE :name OR a.teamName LIKE :inputWithSpace")
})
public class Abbreviation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="team_abbrev")
	private String teamAbbrev;

	@Column(name="team_name")
	private String teamName;

	public Abbreviation() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTeamAbbrev() {
		return this.teamAbbrev;
	}

	public void setTeamAbbrev(String teamAbbrev) {
		this.teamAbbrev = teamAbbrev;
	}

	public String getTeamName() {
		return this.teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

}