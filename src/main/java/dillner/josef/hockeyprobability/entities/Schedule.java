package dillner.josef.hockeyprobability.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the schedule database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Schedule.findAll", query="SELECT s FROM Schedule s"),
	@NamedQuery(name="Schedule.findByDate", query="SELECT s FROM Schedule s WHERE s.date LIKE :date")
})
public class Schedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String date;

	private String home;

	private String visitor;

	public Schedule() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getHome() {
		return this.home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public String getVisitor() {
		return this.visitor;
	}

	public void setVisitor(String visitor) {
		this.visitor = visitor;
	}

}