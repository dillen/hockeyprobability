package dillner.josef.hockeyprobability.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the team_stats_test database table.
 * 
 */
@Entity
@Table(name="team_stats_test")
@NamedQueries({
	@NamedQuery(name="TeamStatsTest.findAll", query="SELECT t FROM TeamStatsTest t"),
	@NamedQuery(name="TeamStatsTest.searchByName", query="SELECT t FROM TeamStatsTest t WHERE t.team LIKE :input OR t.team LIKE :inputWithSpace"),
	@NamedQuery(name="TeamStatsTest.getTeamById", query="SELECT t FROM TeamStatsTest t WHERE t.id = :id")
})
public class TeamStatsTest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="`AdjFSv%`")
	private String adjFSv_;

	private String ca;

	private String ca60;

	private String cf;

	@Column(name="`CF%`")
	private String cf_;

	private String cf60;

	@Column(name="`CSh%`")
	private String CSh_;

	@Column(name="`CSv%`")
	private String CSv_;

	private String fa;

	private String fa60;

	private String ff;

	@Column(name="`FF%`")
	private String ff_;

	private String ff60;

	@Column(name="`FO%`")
	private String fo_;

	@Column(name="`FSh%`")
	private String FSh_;

	@Column(name="`FSv%`")
	private String FSv_;

	private String ga;

	private String ga60;

	private String gf;

	@Column(name="`GF%`")
	private String gf_;

	private String gf60;

	private String gp;

	private String pdo;

	private String pendiff;

	private String sa;

	private String sa60;

	private String sca;

	private String sca60;

	private String scf;

	@Column(name="`SCF%`")
	private String scf_;

	private String scf60;

	private String season;

	private String seasonType;

	private String sf;

	@Column(name="`SF%`")
	private String sf_;

	private String sf60;

	@Column(name="`Sh%`")
	private String sh_;

	@Column(name="`Sv%`")
	private String sv_;

	private String team;

	private String toi;

	@Column(name="`xFSh%`")
	private String xFSh_;

	@Column(name="`xFSv%`")
	private String xFSv_;

	private String xGA;

	private String xGA60;

	private String xGF;

	@Column(name="`xGF%`")
	private String xGF_;

	private String xGF60;

	private String xPDO;

	public TeamStatsTest() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAdjFSv_() {
		return this.adjFSv_;
	}

	public void setAdjFSv_(String adjFSv_) {
		this.adjFSv_ = adjFSv_;
	}

	public String getCa() {
		return this.ca;
	}

	public void setCa(String ca) {
		this.ca = ca;
	}

	public String getCa60() {
		return this.ca60;
	}

	public void setCa60(String ca60) {
		this.ca60 = ca60;
	}

	public String getCf() {
		return this.cf;
	}

	public void setCf(String cf) {
		this.cf = cf;
	}

	public String getCf_() {
		return this.cf_;
	}

	public void setCf_(String cf_) {
		this.cf_ = cf_;
	}

	public String getCf60() {
		return this.cf60;
	}

	public void setCf60(String cf60) {
		this.cf60 = cf60;
	}

	public String getCSh_() {
		return this.CSh_;
	}

	public void setCSh_(String CSh_) {
		this.CSh_ = CSh_;
	}

	public String getCSv_() {
		return this.CSv_;
	}

	public void setCSv_(String CSv_) {
		this.CSv_ = CSv_;
	}

	public String getFa() {
		return this.fa;
	}

	public void setFa(String fa) {
		this.fa = fa;
	}

	public String getFa60() {
		return this.fa60;
	}

	public void setFa60(String fa60) {
		this.fa60 = fa60;
	}

	public String getFf() {
		return this.ff;
	}

	public void setFf(String ff) {
		this.ff = ff;
	}

	public String getFf_() {
		return this.ff_;
	}

	public void setFf_(String ff_) {
		this.ff_ = ff_;
	}

	public String getFf60() {
		return this.ff60;
	}

	public void setFf60(String ff60) {
		this.ff60 = ff60;
	}

	public String getFo_() {
		return this.fo_;
	}

	public void setFo_(String fo_) {
		this.fo_ = fo_;
	}

	public String getFSh_() {
		return this.FSh_;
	}

	public void setFSh_(String FSh_) {
		this.FSh_ = FSh_;
	}

	public String getFSv_() {
		return this.FSv_;
	}

	public void setFSv_(String FSv_) {
		this.FSv_ = FSv_;
	}

	public String getGa() {
		return this.ga;
	}

	public void setGa(String ga) {
		this.ga = ga;
	}

	public String getGa60() {
		return this.ga60;
	}

	public void setGa60(String ga60) {
		this.ga60 = ga60;
	}

	public String getGf() {
		return this.gf;
	}

	public void setGf(String gf) {
		this.gf = gf;
	}

	public String getGf_() {
		return this.gf_;
	}

	public void setGf_(String gf_) {
		this.gf_ = gf_;
	}

	public String getGf60() {
		return this.gf60;
	}

	public void setGf60(String gf60) {
		this.gf60 = gf60;
	}

	public String getGp() {
		return this.gp;
	}

	public void setGp(String gp) {
		this.gp = gp;
	}

	public String getPdo() {
		return this.pdo;
	}

	public void setPdo(String pdo) {
		this.pdo = pdo;
	}

	public String getPendiff() {
		return this.pendiff;
	}

	public void setPendiff(String pendiff) {
		this.pendiff = pendiff;
	}

	public String getSa() {
		return this.sa;
	}

	public void setSa(String sa) {
		this.sa = sa;
	}

	public String getSa60() {
		return this.sa60;
	}

	public void setSa60(String sa60) {
		this.sa60 = sa60;
	}

	public String getSca() {
		return this.sca;
	}

	public void setSca(String sca) {
		this.sca = sca;
	}

	public String getSca60() {
		return this.sca60;
	}

	public void setSca60(String sca60) {
		this.sca60 = sca60;
	}

	public String getScf() {
		return this.scf;
	}

	public void setScf(String scf) {
		this.scf = scf;
	}

	public String getScf_() {
		return this.scf_;
	}

	public void setScf_(String scf_) {
		this.scf_ = scf_;
	}

	public String getScf60() {
		return this.scf60;
	}

	public void setScf60(String scf60) {
		this.scf60 = scf60;
	}

	public String getSeason() {
		return this.season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public String getSeasonType() {
		return this.seasonType;
	}

	public void setSeasonType(String seasonType) {
		this.seasonType = seasonType;
	}

	public String getSf() {
		return this.sf;
	}

	public void setSf(String sf) {
		this.sf = sf;
	}

	public String getSf_() {
		return this.sf_;
	}

	public void setSf_(String sf_) {
		this.sf_ = sf_;
	}

	public String getSf60() {
		return this.sf60;
	}

	public void setSf60(String sf60) {
		this.sf60 = sf60;
	}

	public String getSh_() {
		return this.sh_;
	}

	public void setSh_(String sh_) {
		this.sh_ = sh_;
	}

	public String getSv_() {
		return this.sv_;
	}

	public void setSv_(String sv_) {
		this.sv_ = sv_;
	}

	public String getTeam() {
		return this.team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getToi() {
		return this.toi;
	}

	public void setToi(String toi) {
		this.toi = toi;
	}

	public String getXFSh_() {
		return this.xFSh_;
	}

	public void setXFSh_(String xFSh_) {
		this.xFSh_ = xFSh_;
	}

	public String getXFSv_() {
		return this.xFSv_;
	}

	public void setXFSv_(String xFSv_) {
		this.xFSv_ = xFSv_;
	}

	public String getXGA() {
		return this.xGA;
	}

	public void setXGA(String xGA) {
		this.xGA = xGA;
	}

	public String getXGA60() {
		return this.xGA60;
	}

	public void setXGA60(String xGA60) {
		this.xGA60 = xGA60;
	}

	public String getXGF() {
		return this.xGF;
	}

	public void setXGF(String xGF) {
		this.xGF = xGF;
	}

	public String getXGF_() {
		return this.xGF_;
	}

	public void setXGF_(String xGF_) {
		this.xGF_ = xGF_;
	}

	public String getXGF60() {
		return this.xGF60;
	}

	public void setXGF60(String xGF60) {
		this.xGF60 = xGF60;
	}

	public String getXPDO() {
		return this.xPDO;
	}

	public void setXPDO(String xPDO) {
		this.xPDO = xPDO;
	}

}