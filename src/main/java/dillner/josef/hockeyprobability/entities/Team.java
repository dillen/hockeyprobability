package dillner.josef.hockeyprobability.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the team database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Team.findAll", query="SELECT t FROM Team t"),
	@NamedQuery(name="Team.findByName", query="SELECT t FROM Team t WHERE t.teamName LIKE :name"),
	@NamedQuery(name="Team.findById", query="SELECT t FROM Team t WHERE t.id = :id")
})
public class Team implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="actual_goals_for")
	private double actualGoalsFor;

	@Column(name="expected_goals_for")
	private double expectedGoalsFor;

	@Column(name="last_game_goals_for")
	private double lastGameGoalsFor;
	
	@Column(name="result_AGF")
	private double resultAGF;

	@Column(name="result_last_GF")
	private double resultLastGF;

	@Column(name="result_XGF")
	private double resultXGF;

	@Column(name="team_name")
	private String teamName;

	public Team() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getActualGoalsFor() {
		return this.actualGoalsFor;
	}

	public void setActualGoalsFor(double actualGoalsFor) {
		this.actualGoalsFor = actualGoalsFor;
	}

	public double getExpectedGoalsFor() {
		return this.expectedGoalsFor;
	}

	public void setExpectedGoalsFor(double expectedGoalsFor) {
		this.expectedGoalsFor = expectedGoalsFor;
	}

	public double getLastGameGoalsFor() {
		return this.lastGameGoalsFor;
	}

	public void setLastGameGoalsFor(double lastGameGoalsFor) {
		this.lastGameGoalsFor = lastGameGoalsFor;
	}

	public double getResultAGF() {
		return resultAGF;
	}

	public void setResultAGF(double resultAGF) {
		this.resultAGF = resultAGF;
	}

	public double getResultLastGF() {
		return resultLastGF;
	}

	public void setResultLastGF(double resultLastGF) {
		this.resultLastGF = resultLastGF;
	}

	public double getResultXGF() {
		return resultXGF;
	}

	public void setResultXGF(double resultXGF) {
		this.resultXGF = resultXGF;
	}

	public String getTeamName() {
		return this.teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

}