package dillner.josef.hockeyprobability.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the team_stats database table.
 * 
 */
@Entity
@Table(name="team_stats")
@NamedQueries({
	@NamedQuery(name="TeamStat.findAll", query="SELECT t FROM TeamStat t"),
	@NamedQuery(name="TeamStat.findByName", query="SELECT t FROM TeamStat t WHERE t.team LIKE :input OR t.team LIKE :inputWithSpace"),
	@NamedQuery(name="TeamStat.findById", query="SELECT t FROM TeamStat t WHERE t.id = :id")
})
public class TeamStat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="`AdjFSv%`")
	private double adjFSv_;

	private int ca;

	private double ca60;

	private int cf;

	@Column(name="`CF%`", columnDefinition="double")
	private Double cf_;

	private double cf60;

	@Column(name="`CSh%`")
	private double CSh_;

	@Column(name="`CSv%`")
	private double CSv_;

	private int fa;

	private double fa60;

	private int ff;

	@Column(name="`FF%`")
	private double ff_;

	private double ff60;

	@Column(name="`FO%`")
	private double fo_;

	@Column(name="`FSh%`")
	private double FSh_;

	@Column(name="`FSv%`")
	private double FSv_;

	private double ga;

	private double ga60;

	private double gf;

	@Column(name="`GF%`")
	private double gf_;

	private double gf60;

	private int gp;

	private double pdo;

	private double pendiff;

	private int sa;

	private double sa60;

	private int sca;

	private double sca60;

	private int scf;

	@Column(name="`SCF%`")
	private double scf_;

	private double scf60;

	private String season;

	private String seasonType;

	private int sf;

	@Column(name="`SF%`")
	private double sf_;

	private double sf60;

	@Column(name="`Sh%`")
	private double sh_;

	@Column(name="`Sv%`")
	private double sv_;

	private String team;

	private double toi;

	@Column(name="`xFSh%`")
	private double xFSh_;

	@Column(name="`xFSv%`")
	private double xFSv_;

	private double xGA;

	private double xGA60;

	private double xGF;

	@Column(name="`xGF%`")
	private double xGF_;

	private double xGF60;

	private double xPDO;

	public TeamStat() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getAdjFSv_() {
		return this.adjFSv_;
	}

	public void setAdjFSv_(double adjFSv_) {
		this.adjFSv_ = adjFSv_;
	}

	public int getCa() {
		return this.ca;
	}

	public void setCa(int ca) {
		this.ca = ca;
	}

	public double getCa60() {
		return this.ca60;
	}

	public void setCa60(double ca60) {
		this.ca60 = ca60;
	}

	public int getCf() {
		return this.cf;
	}

	public void setCf(int cf) {
		this.cf = cf;
	}

	public Double getCf_() {
		return this.cf_;
	}

	public void setCf_(Double cf_) {
		this.cf_ = cf_;
	}

	public double getCf60() {
		return this.cf60;
	}

	public void setCf60(double cf60) {
		this.cf60 = cf60;
	}

	public double getCSh_() {
		return this.CSh_;
	}

	public void setCSh_(double CSh_) {
		this.CSh_ = CSh_;
	}

	public double getCSv_() {
		return this.CSv_;
	}

	public void setCSv_(double CSv_) {
		this.CSv_ = CSv_;
	}

	public int getFa() {
		return this.fa;
	}

	public void setFa(int fa) {
		this.fa = fa;
	}

	public double getFa60() {
		return this.fa60;
	}

	public void setFa60(double fa60) {
		this.fa60 = fa60;
	}

	public int getFf() {
		return this.ff;
	}

	public void setFf(int ff) {
		this.ff = ff;
	}

	public double getFf_() {
		return this.ff_;
	}

	public void setFf_(double ff_) {
		this.ff_ = ff_;
	}

	public double getFf60() {
		return this.ff60;
	}

	public void setFf60(double ff60) {
		this.ff60 = ff60;
	}

	public double getFo_() {
		return this.fo_;
	}

	public void setFo_(double fo_) {
		this.fo_ = fo_;
	}

	public double getFSh_() {
		return this.FSh_;
	}

	public void setFSh_(double FSh_) {
		this.FSh_ = FSh_;
	}

	public double getFSv_() {
		return this.FSv_;
	}

	public void setFSv_(double FSv_) {
		this.FSv_ = FSv_;
	}

	public double getGa() {
		return this.ga;
	}

	public void setGa(double ga) {
		this.ga = ga;
	}

	public double getGa60() {
		return this.ga60;
	}

	public void setGa60(double ga60) {
		this.ga60 = ga60;
	}

	public double getGf() {
		return this.gf;
	}

	public void setGf(double gf) {
		this.gf = gf;
	}

	public double getGf_() {
		return this.gf_;
	}

	public void setGf_(double gf_) {
		this.gf_ = gf_;
	}

	public double getGf60() {
		return this.gf60;
	}

	public void setGf60(double gf60) {
		this.gf60 = gf60;
	}

	public int getGp() {
		return this.gp;
	}

	public void setGp(int gp) {
		this.gp = gp;
	}

	public double getPdo() {
		return this.pdo;
	}

	public void setPdo(double pdo) {
		this.pdo = pdo;
	}

	public double getPendiff() {
		return this.pendiff;
	}

	public void setPendiff(double pendiff) {
		this.pendiff = pendiff;
	}

	public int getSa() {
		return this.sa;
	}

	public void setSa(int sa) {
		this.sa = sa;
	}

	public double getSa60() {
		return this.sa60;
	}

	public void setSa60(double sa60) {
		this.sa60 = sa60;
	}

	public int getSca() {
		return this.sca;
	}

	public void setSca(int sca) {
		this.sca = sca;
	}

	public double getSca60() {
		return this.sca60;
	}

	public void setSca60(double sca60) {
		this.sca60 = sca60;
	}

	public int getScf() {
		return this.scf;
	}

	public void setScf(int scf) {
		this.scf = scf;
	}

	public double getScf_() {
		return this.scf_;
	}

	public void setScf_(double scf_) {
		this.scf_ = scf_;
	}

	public double getScf60() {
		return this.scf60;
	}

	public void setScf60(double scf60) {
		this.scf60 = scf60;
	}

	public String getSeason() {
		return this.season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public String getSeasonType() {
		return this.seasonType;
	}

	public void setSeasonType(String seasonType) {
		this.seasonType = seasonType;
	}

	public int getSf() {
		return this.sf;
	}

	public void setSf(int sf) {
		this.sf = sf;
	}

	public double getSf_() {
		return this.sf_;
	}

	public void setSf_(double sf_) {
		this.sf_ = sf_;
	}

	public double getSf60() {
		return this.sf60;
	}

	public void setSf60(double sf60) {
		this.sf60 = sf60;
	}

	public double getSh_() {
		return this.sh_;
	}

	public void setSh_(double sh_) {
		this.sh_ = sh_;
	}

	public double getSv_() {
		return this.sv_;
	}

	public void setSv_(double sv_) {
		this.sv_ = sv_;
	}

	public String getTeam() {
		return this.team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public double getToi() {
		return this.toi;
	}

	public void setToi(double toi) {
		this.toi = toi;
	}

	public double getXFSh_() {
		return this.xFSh_;
	}

	public void setXFSh_(double xFSh_) {
		this.xFSh_ = xFSh_;
	}

	public double getXFSv_() {
		return this.xFSv_;
	}

	public void setXFSv_(double xFSv_) {
		this.xFSv_ = xFSv_;
	}

	public double getXGA() {
		return this.xGA;
	}

	public void setXGA(double xGA) {
		this.xGA = xGA;
	}

	public double getXGA60() {
		return this.xGA60;
	}

	public void setXGA60(double xGA60) {
		this.xGA60 = xGA60;
	}

	public double getXGF() {
		return this.xGF;
	}

	public void setXGF(double xGF) {
		this.xGF = xGF;
	}

	public double getXGF_() {
		return this.xGF_;
	}

	public void setXGF_(double xGF_) {
		this.xGF_ = xGF_;
	}

	public double getXGF60() {
		return this.xGF60;
	}

	public void setXGF60(double xGF60) {
		this.xGF60 = xGF60;
	}

	public double getXPDO() {
		return this.xPDO;
	}

	public void setXPDO(double xPDO) {
		this.xPDO = xPDO;
	}

}