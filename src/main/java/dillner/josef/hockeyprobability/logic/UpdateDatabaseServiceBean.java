package dillner.josef.hockeyprobability.logic;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import dillner.josef.hockeyprobability.entities.Team;
import dillner.josef.hockeyprobability.entities.TeamStat;
import dillner.josef.hockeyprobability.persistence.PersistenceManager;
import dillner.josef.hockeyprobability.util.CsvReader;

@ApplicationScoped
public class UpdateDatabaseServiceBean implements UpdateDatabaseService {

	@Inject
	private PersistenceManager persistenceManager;
	
	@Override
	public String updateDatabase() throws Exception {
		String updateCsvStats = updateCsvStats();
		String updateTeamStats = updateTeamStats();
		return updateCsvStats+" and " +updateTeamStats;
	}
	
	private String updateTeamStats() throws Exception{
		List<TeamStat> teamStats = persistenceManager.getAllTeamStats();
		List<Team> listOfTeams = persistenceManager.getAllTeams();
		for (int i = 0; i < teamStats.size(); i++) {
			for (int j = 0; j < listOfTeams.size(); j++) {
				if(teamStats.get(i).getTeam().equals(listOfTeams.get(j).getTeamName())) {
					listOfTeams.get(j).setExpectedGoalsFor(teamStats.get(i).getXGF60());
					listOfTeams.get(j).setActualGoalsFor(teamStats.get(i).getGf60());
				}else {
				}
			}
		}
		return "Team stats updated";
	}
	
	private String updateCsvStats()throws Exception{
		CsvReader csvReader = new CsvReader();
		List<String[]> stats = csvReader.csvParser();
		final List<TeamStat> updatedStats = new ArrayList<>();
		TeamStat teamStat = null;
		for (int i = 1; i < stats.size(); i++) {
			for (int j = 0; j < stats.get(i).length; j++) {
				teamStat = new TeamStat();
				teamStat = persistenceManager.getTeamStatById(i);
				teamStat.setGp(Integer.parseInt(stats.get(i)[4]));
				teamStat.setToi(Double.parseDouble(stats.get(i)[5]));
				teamStat.setCf(Integer.parseInt(stats.get(i)[6]));
				teamStat.setCa(Integer.parseInt(stats.get(i)[7]));
				teamStat.setCf60(Double.parseDouble(stats.get(i)[8]));
				teamStat.setCa60(Double.parseDouble(stats.get(i)[9]));
				teamStat.setCf_(Double.parseDouble(stats.get(i)[10]));
				teamStat.setCSh_(Double.parseDouble(stats.get(i)[11]));
				teamStat.setCSv_(Double.parseDouble(stats.get(i)[12]));
				teamStat.setFf(Integer.parseInt(stats.get(i)[13]));
				teamStat.setFa(Integer.parseInt(stats.get(i)[14]));
				teamStat.setFf60(Double.parseDouble(stats.get(i)[15]));
				teamStat.setFa60(Double.parseDouble(stats.get(i)[16]));
				teamStat.setFf_(Double.parseDouble(stats.get(i)[17]));
				teamStat.setFSh_(Double.parseDouble(stats.get(i)[18]));
				teamStat.setFSv_(Double.parseDouble(stats.get(i)[19]));
				teamStat.setSf(Integer.parseInt(stats.get(i)[20]));
				teamStat.setSa(Integer.parseInt(stats.get(i)[21]));
				teamStat.setSf60(Double.parseDouble(stats.get(i)[22]));
				teamStat.setSa60(Double.parseDouble(stats.get(i)[23]));
				teamStat.setSf_(Double.parseDouble(stats.get(i)[24]));
				teamStat.setSh_(Double.parseDouble(stats.get(i)[25]));
				teamStat.setSv_(Double.parseDouble(stats.get(i)[26]));
				teamStat.setXGF(Double.parseDouble(stats.get(i)[27]));
				teamStat.setXGA(Double.parseDouble(stats.get(i)[28]));
				teamStat.setXGF60(Double.parseDouble(stats.get(i)[29]));
				teamStat.setXGA60(Double.parseDouble(stats.get(i)[30]));
				teamStat.setXGF_(Double.parseDouble(stats.get(i)[31]));
				teamStat.setScf(Integer.parseInt(stats.get(i)[32]));
				teamStat.setSca(Integer.parseInt(stats.get(i)[33]));
				teamStat.setScf60(Double.parseDouble(stats.get(i)[34]));
				teamStat.setScf_(Double.parseDouble(stats.get(i)[35]));
				teamStat.setXFSh_(Double.parseDouble(stats.get(i)[36]));
				teamStat.setXFSv_(Double.parseDouble(stats.get(i)[37]));
				teamStat.setAdjFSv_(Double.parseDouble(stats.get(i)[38]));
				teamStat.setPdo(Double.parseDouble(stats.get(i)[39]));
				teamStat.setXPDO(Double.parseDouble(stats.get(i)[40]));
				teamStat.setGf(Double.parseDouble(stats.get(i)[41]));
				teamStat.setGa(Double.parseDouble(stats.get(i)[42]));
				teamStat.setGf60(Double.parseDouble(stats.get(i)[43]));
				teamStat.setGa60(Double.parseDouble(stats.get(i)[44]));
				teamStat.setGf_(Double.parseDouble(stats.get(i)[45]));
				teamStat.setFo_(Double.parseDouble(stats.get(i)[46]));
				teamStat.setPendiff(Double.parseDouble(stats.get(i)[47]));
				updatedStats.add(teamStat);
			}
		}
		persistenceManager.storeTeamStats(updatedStats);
		return "CSV updated";
	}

}
