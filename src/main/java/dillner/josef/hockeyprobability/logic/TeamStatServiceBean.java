package dillner.josef.hockeyprobability.logic;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import dillner.josef.hockeyprobability.calculations.CalculateProbability;
import dillner.josef.hockeyprobability.calculations.MoreParameters;
import dillner.josef.hockeyprobability.domainobject.GameDO;
import dillner.josef.hockeyprobability.entities.Abbreviation;
import dillner.josef.hockeyprobability.entities.Schedule;
import dillner.josef.hockeyprobability.logic.exception.PersistenceOperationException;
import dillner.josef.hockeyprobability.persistence.PersistenceManager;
import dillner.josef.hockeyprobability.util.TwitterUtil;
import twitter4j.TwitterException;

@ApplicationScoped
public class TeamStatServiceBean implements TeamStatService {

	@Inject
	private PersistenceManager persistenceManager;
	
	@Inject
	private CalculateProbability calculateProbability;
	
	@Inject
	private MoreParameters moreParameters;
	
	@Inject
	private TwitterUtil twitterUtil;
	
	@Inject
	private Logger logger;
	
	//TODO uncomment twitter
	@Override
	public List<GameDO> calculateTeamProbability() throws PersistenceOperationException {
		final List<GameDO> games = getTodaysGames();
		final List<GameDO> calculatedGames = moreParameters.extraParameters(calculateProbability.calculateMyGuess(games));
		/*try {
			twitterUtil.updateTwitterStatus(calculatedGames);
		} catch (TwitterException e) {
			this.logger.severe("Twitter failed: " + e.getMessage());
		}*/
		return calculatedGames;
	}

	//Season is over so setting date to april 8 to get the most results
	private List<GameDO> getTodaysGames() throws PersistenceOperationException  {
		//final List<Schedule> listOfTodaysGames = persistenceManager.getListOfTeams(getTodaysDate());
		final List<Schedule> listOfTodaysGames = persistenceManager.getListOfTeams("2017-04-06");
		final List<GameDO> listOfGames = new ArrayList<GameDO>();
		GameDO gameDO = null;
		Abbreviation abbreviationTeamOne = null;
		Abbreviation abbreviationTeamTwo = null;
		for (int i = 0; i < listOfTodaysGames.size(); i++) {
			gameDO = new GameDO();
			abbreviationTeamOne = new Abbreviation();
			abbreviationTeamTwo = new Abbreviation();
			abbreviationTeamOne = persistenceManager.findByName(listOfTodaysGames.get(i).getHome());
			abbreviationTeamTwo = persistenceManager.findByName(listOfTodaysGames.get(i).getVisitor());
			gameDO.setHomeTeamName(listOfTodaysGames.get(i).getHome());
			gameDO.setAwayTeamName(listOfTodaysGames.get(i).getVisitor());
			gameDO.setHomeTeam(persistenceManager.getTeamByName(abbreviationTeamOne.getTeamAbbrev()));
			gameDO.setAwayTeam(persistenceManager.getTeamByName(abbreviationTeamTwo.getTeamAbbrev()));
			listOfGames.add(gameDO);
			
		}
		return listOfGames;
	}
	
	//Todays date is adjusted to North American eastern time
	private String getTodaysDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone("EST"));
		Date date = calendar.getTime();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return simpleDateFormat.format(date);
	}
}
