package dillner.josef.hockeyprobability.logic.exception;

public class PersistenceOperationException extends Exception {
	private static final long serialVersionUID = 1L;

	public PersistenceOperationException(String message) {
		super(message);
	}

}
