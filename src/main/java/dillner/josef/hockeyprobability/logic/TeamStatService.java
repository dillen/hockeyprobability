package dillner.josef.hockeyprobability.logic;

import java.util.List;

import dillner.josef.hockeyprobability.domainobject.GameDO;

public interface TeamStatService {

	public List<GameDO> calculateTeamProbability() throws Exception;

}
