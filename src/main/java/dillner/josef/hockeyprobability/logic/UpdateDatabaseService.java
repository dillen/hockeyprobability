package dillner.josef.hockeyprobability.logic;

public interface UpdateDatabaseService {

	public String updateDatabase() throws Exception;

}
