package dillner.josef.hockeyprobability.calculations;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import dillner.josef.hockeyprobability.domainobject.GameDO;
import dillner.josef.hockeyprobability.entities.TeamStat;
import dillner.josef.hockeyprobability.logic.exception.PersistenceOperationException;
import dillner.josef.hockeyprobability.persistence.PersistenceManager;

@Stateless
public class MoreParameters {

	@Inject
	private PersistenceManager persistenceManager;

	public List<GameDO> extraParameters(List<GameDO> games) throws PersistenceOperationException {
		double cfHome;
		double cfAway;
		double ffHome;
		double ffAway;
		double goalieHome;
		double goalieAway;
		double sfHome;
		double sfAway;
		double shHome;
		double shAway;

		for (int i = 0; i < games.size(); i++) {
			TeamStat teamStatHome = new TeamStat();
			TeamStat teamStatAway = new TeamStat();
			teamStatHome = persistenceManager.getTeamStatsByName(games.get(i).getHomeTeam().getTeamName());
			teamStatAway = persistenceManager.getTeamStatsByName(games.get(i).getAwayTeam().getTeamName());

			if (teamStatHome.getCf_() > teamStatAway.getCf_()) {
				cfHome = games.get(i).getHomeTeamChance() + calculateCf(teamStatHome, teamStatAway);
				cfAway = games.get(i).getAwayTeamChance() - calculateCf(teamStatHome, teamStatAway);
				games.get(i).setHomeTeamChance(cfHome);
				games.get(i).setAwayTeamChance(cfAway);
			} else {
				cfHome = games.get(i).getHomeTeamChance() + calculateCf(teamStatHome, teamStatAway);
				cfAway = games.get(i).getAwayTeamChance() - calculateCf(teamStatHome, teamStatAway);
				games.get(i).setHomeTeamChance(cfHome);
				games.get(i).setAwayTeamChance(cfAway);
			}
			if (teamStatHome.getFf_() > teamStatAway.getFf_()) {
				ffHome = games.get(i).getHomeTeamChance() + calculateFf(teamStatHome, teamStatAway);
				ffAway = games.get(i).getAwayTeamChance() - calculateFf(teamStatHome, teamStatAway);
				games.get(i).setHomeTeamChance(ffHome);
				games.get(i).setAwayTeamChance(ffAway);
			} else {
				ffHome = games.get(i).getHomeTeamChance() + calculateFf(teamStatHome, teamStatAway);
				ffAway = games.get(i).getAwayTeamChance() - calculateFf(teamStatHome, teamStatAway);
				games.get(i).setHomeTeamChance(ffHome);
				games.get(i).setAwayTeamChance(ffAway);
			}
			if (teamStatHome.getSv_() > teamStatAway.getSv_()) {
				goalieHome = games.get(i).getHomeTeamChance() + calculateGoalie(teamStatHome, teamStatAway);
				goalieAway = games.get(i).getAwayTeamChance() - calculateGoalie(teamStatHome, teamStatAway);
				games.get(i).setHomeTeamChance(goalieHome);
				games.get(i).setAwayTeamChance(goalieAway);
			} else {
				goalieHome = games.get(i).getHomeTeamChance() + calculateGoalie(teamStatHome, teamStatAway);
				goalieAway = games.get(i).getAwayTeamChance() - calculateGoalie(teamStatHome, teamStatAway);
				games.get(i).setHomeTeamChance(goalieHome);
				games.get(i).setAwayTeamChance(goalieAway);
			}
			if (teamStatHome.getSf_() > teamStatAway.getSf_()) {
				sfHome = games.get(i).getHomeTeamChance() + caculateShotsOnGoalPercentage(teamStatHome, teamStatAway);
				sfAway = games.get(i).getAwayTeamChance() - caculateShotsOnGoalPercentage(teamStatHome, teamStatAway);
				games.get(i).setHomeTeamChance(sfHome);
				games.get(i).setAwayTeamChance(sfAway);
			} else {
				sfHome = games.get(i).getHomeTeamChance() + caculateShotsOnGoalPercentage(teamStatHome, teamStatAway);
				sfAway = games.get(i).getAwayTeamChance() - caculateShotsOnGoalPercentage(teamStatHome, teamStatAway);
				games.get(i).setHomeTeamChance(sfHome);
				games.get(i).setAwayTeamChance(sfAway);
			}
			if (teamStatHome.getSh_() > teamStatAway.getSh_()) {
				shHome = games.get(i).getHomeTeamChance() + caculateShootingPercentage(teamStatHome, teamStatAway);
				shAway = games.get(i).getAwayTeamChance() - caculateShootingPercentage(teamStatHome, teamStatAway);
				games.get(i).setHomeTeamChance(shHome);
				games.get(i).setAwayTeamChance(shAway);
			} else {
				shHome = games.get(i).getHomeTeamChance() + caculateShootingPercentage(teamStatHome, teamStatAway);
				shAway = games.get(i).getAwayTeamChance() - caculateShootingPercentage(teamStatHome, teamStatAway);
				games.get(i).setHomeTeamChance(shHome);
				games.get(i).setAwayTeamChance(shAway);
			}
		}
		return games;
	}

	//Calculate Corsi
	private Double calculateCf(TeamStat homeTeam, TeamStat awayTeam) {
		double result = homeTeam.getCf_() - awayTeam.getCf_();
		return roundNumbers(result);
	}

	// Calculate fencwick
	private Double calculateFf(TeamStat homeTeam, TeamStat awayTeam) {
		double result = homeTeam.getFf_() - awayTeam.getFf_();
		return roundNumbers(result);
	}

	//Calculate goali save percentage.
	private Double calculateGoalie(TeamStat homeTeam, TeamStat awayTeam) {
		double result = homeTeam.getSv_() - awayTeam.getSv_();
		return roundNumbers(result);
	}

	// shoots on goal
	private Double caculateShotsOnGoalPercentage(TeamStat homeTeam, TeamStat awayTeam) {
		double result = homeTeam.getSf_() - awayTeam.getSf_();
		return roundNumbers(result);
	}

	//calculate shooting percentage.
	private Double caculateShootingPercentage(TeamStat homeTeam, TeamStat awayTeam) {
		double result = homeTeam.getSh_() - awayTeam.getSh_();
		return roundNumbers(result);
	}

	private Double roundNumbers(Double result) {
		double roundedNumbers = Math.round(result * 1d);
		roundedNumbers = roundedNumbers / 1d;
		return roundedNumbers;
	}

}
