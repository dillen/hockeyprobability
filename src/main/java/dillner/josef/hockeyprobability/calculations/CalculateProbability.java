package dillner.josef.hockeyprobability.calculations;

import java.util.List;

import dillner.josef.hockeyprobability.domainobject.GameDO;

public class CalculateProbability {

	
	public List<GameDO> calculateMyGuess(List<GameDO> games) {
		for (int i = 0; i < games.size(); i++) {
			calculateXGF(games.get(i));
			calculateaGF(games.get(i));
			games.get(i).setHomeTeamChance(
					(games.get(i).getHomeTeam().getResultXGF() + games.get(i).getHomeTeam().getResultAGF()) / 2);
			games.get(i).setAwayTeamChance(
					(games.get(i).getAwayTeam().getResultXGF() + games.get(i).getAwayTeam().getResultAGF()) / 2);
		}
		return games;
	}

	// Calculate expected goals for both home- and away-team
	private GameDO calculateXGF(GameDO game) {
		double result = (powerOf(game.getHomeTeam().getExpectedGoalsFor()))
				/ (powerOf(game.getHomeTeam().getExpectedGoalsFor())
						+ powerOf(game.getAwayTeam().getExpectedGoalsFor()));
		double result2 = (powerOf(game.getAwayTeam().getExpectedGoalsFor()))
				/ (powerOf(game.getAwayTeam().getExpectedGoalsFor())
						+ powerOf(game.getHomeTeam().getExpectedGoalsFor()));

		game.getHomeTeam().setResultXGF(roundNumbers(result));
		game.getAwayTeam().setResultXGF(roundNumbers(result2));
		return game;
	}

	// Calculate actual goals for both home- and away-team
	private GameDO calculateaGF(GameDO game) {
		double result = (powerOf(game.getHomeTeam().getActualGoalsFor()))
				/ (powerOf(game.getHomeTeam().getActualGoalsFor()) + powerOf(game.getAwayTeam().getActualGoalsFor()));
		double result2 = (powerOf(game.getAwayTeam().getActualGoalsFor()))
				/ (powerOf(game.getAwayTeam().getActualGoalsFor()) + powerOf(game.getHomeTeam().getActualGoalsFor()));

		game.getHomeTeam().setResultAGF(roundNumbers(result));
		game.getAwayTeam().setResultAGF(roundNumbers(result2));
		return game;
	}

	private Double powerOf(Double number) {
		return Math.pow(number, 2.15);
	}

	private Double roundNumbers(Double result) {
		double roundedNumbers = Math.round(result * 10000);
		roundedNumbers = roundedNumbers / 10000;
		return convertToPercent(roundedNumbers);
	}

	private Double convertToPercent(Double result) {
		return result * 100;
	}

}
