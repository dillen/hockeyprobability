package dillner.josef.hockeyprobability.persistence;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;

import dillner.josef.hockeyprobability.entities.Abbreviation;
import dillner.josef.hockeyprobability.entities.Schedule;
import dillner.josef.hockeyprobability.entities.Team;
import dillner.josef.hockeyprobability.entities.TeamStat;
import dillner.josef.hockeyprobability.logic.exception.PersistenceOperationException;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.NEVER)
@Interceptors(ExceptionInterceptor.class)
public class PersistenceManagerBean implements PersistenceManager {

	@Inject
	private EntityManager entityManager;

	@Inject
	private Logger logger;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public String store(Abbreviation abbreviation) throws PersistenceOperationException {
		try {
			entityManager.persist(abbreviation);
			return "abbreviation successfully stored!";
		} catch (Exception e) {
			this.logger.severe("Failed to store abbrevation: " + e.getMessage());
			return "it's burning";
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Abbreviation> getAllAbbreviations() throws PersistenceOperationException {
		return entityManager.createNamedQuery("Abbrevation.findAll", Abbreviation.class)
				.setLockMode(LockModeType.OPTIMISTIC).getResultList();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Abbreviation findByName(String query) throws PersistenceOperationException {
		return entityManager.createNamedQuery("Abbreviation.findByName", Abbreviation.class).setParameter("name", query)
				.setParameter("inputWithSpace", "% " + query + "%").getSingleResult();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Schedule> getListOfTeams(String query) throws PersistenceOperationException {
		List<Schedule> todaysGames = fetchListOfTeams(query);
		return todaysGames;
	}

	private List<Schedule> fetchListOfTeams(String query) {
		final TypedQuery<Schedule> typedQuery = this.entityManager.createNamedQuery("Schedule.findByDate",
				Schedule.class);
		typedQuery.setParameter("date", query);
		List<Schedule> todaysGames = typedQuery.getResultList();

		return todaysGames;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public String storeTeam(Team team) throws PersistenceOperationException {
		try {
			entityManager.persist(team);
			return "team stored";
		} catch (Exception e) {
			this.logger.severe("Failed to store team: " + e.getMessage());
			return "team could not be stored";
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Team getTeamByName(String query) throws PersistenceOperationException {
		return entityManager.createNamedQuery("Team.findByName", Team.class).setParameter("name", query)
				.getSingleResult();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Team getTeamById(Integer id) throws PersistenceOperationException {
		return entityManager.createNamedQuery("Team.findById", Team.class).setParameter("id", id).getSingleResult();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Team> getAllTeams() throws PersistenceOperationException {
		return entityManager.createNamedQuery("Team.findAll", Team.class).getResultList();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public String updateTeamStats(TeamStat teamStat) throws PersistenceOperationException {
		try {
			entityManager.persist(teamStat);
			return teamStat.getTeam() + " updated";
		} catch (Exception e) {
			this.logger.severe("Failed to update teamStats: " + e.getMessage());
			return teamStat.getTeam() + " failed to update";
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public TeamStat getTeamStatsByName(String teamName) throws PersistenceOperationException {
		return entityManager.createNamedQuery("TeamStat.findByName", TeamStat.class).setParameter("input", teamName)
				.setParameter("inputWithSpace", " %" + teamName + "%").getSingleResult();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public TeamStat getTeamStatById(Integer id) throws PersistenceOperationException {
		return entityManager.createNamedQuery("TeamStat.findById", TeamStat.class).setParameter("id", id)
				.getSingleResult();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<TeamStat> getAllTeamStats() throws PersistenceOperationException {
		return entityManager.createNamedQuery("TeamStat.findAll", TeamStat.class).getResultList();

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Schedule> getAllSchedule() throws PersistenceOperationException {
		return entityManager.createNamedQuery("Schedule.findAll", Schedule.class).getResultList();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void storeTeamStats(List<TeamStat> updatedStats) {
		for (int i = 0; i < updatedStats.size(); i++) {
			entityManager.merge(updatedStats.get(i));
		}

	}

}
