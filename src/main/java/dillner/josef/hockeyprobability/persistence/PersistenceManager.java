package dillner.josef.hockeyprobability.persistence;

import java.util.List;

import dillner.josef.hockeyprobability.entities.Abbreviation;
import dillner.josef.hockeyprobability.entities.Schedule;
import dillner.josef.hockeyprobability.entities.Team;
import dillner.josef.hockeyprobability.entities.TeamStat;
import dillner.josef.hockeyprobability.logic.exception.PersistenceOperationException;

public interface PersistenceManager {

	public String store(Abbreviation abbreviation) throws PersistenceOperationException;
	
	public List<Abbreviation> getAllAbbreviations() throws PersistenceOperationException;
	
	public Abbreviation findByName(String query) throws PersistenceOperationException;
	
	public List<Schedule> getListOfTeams(String query) throws PersistenceOperationException;
	
	public String storeTeam(Team team) throws PersistenceOperationException;
	
	public Team getTeamByName(String query) throws PersistenceOperationException;
	
	public Team getTeamById(Integer id) throws PersistenceOperationException;
	
	public List<Team> getAllTeams() throws PersistenceOperationException;
	
	public String updateTeamStats(TeamStat teamStat) throws PersistenceOperationException;
	
	public TeamStat getTeamStatsByName(String teamName) throws PersistenceOperationException;
	
	public TeamStat getTeamStatById(Integer id) throws PersistenceOperationException;
	
	public List<TeamStat> getAllTeamStats() throws PersistenceOperationException;
	
	public List<Schedule> getAllSchedule() throws PersistenceOperationException;

	public void storeTeamStats(List<TeamStat> updatedStats);
	
}
