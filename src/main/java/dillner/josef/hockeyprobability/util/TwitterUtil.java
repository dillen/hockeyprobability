package dillner.josef.hockeyprobability.util;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import dillner.josef.hockeyprobability.domainobject.GameDO;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

@Stateless
public class TwitterUtil {

	@Inject
	private Logger logger;
	
	@Inject
	private TwitterKeyReader twitterKeyReader;
	
	public String updateTwitterStatus(List<GameDO> twitterStatus) throws TwitterException {
		try {
			String consumerKey = twitterKeyReader.getKey("consumer_key");
			String consumerSecret = twitterKeyReader.getKey("consumer_secret");
			String accessToken = twitterKeyReader.getKey("access_token");
			String accessTokenSecret = twitterKeyReader.getKey("access_token_secret");
			Twitter twitter = TwitterFactory.getSingleton();
			twitter.setOAuthConsumer(consumerKey, consumerSecret);
			twitter.setOAuthAccessToken(new AccessToken(accessToken, accessTokenSecret));
			String tweet = null;
			for (int i = 0; i < twitterStatus.size(); i++) {
				twitterStatus.get(i).getHomeTeam();
				twitterStatus.get(i).getHomeTeamChance();
				twitterStatus.get(i).getAwayTeam();
				twitterStatus.get(i).getAwayTeamChance();
				tweet = "Home: " + twitterStatus.get(i).getHomeTeam().getTeamName()+" "+twitterStatus.get(i).getHomeTeamChance()+" / "+"Away: " + twitterStatus.get(i).getAwayTeam().getTeamName() + " "+
				twitterStatus.get(i).getAwayTeamChance();
				twitter.updateStatus(tweet);
			}
			System.out.println("Updated status");
			return "updated status";
		} catch (Exception e) {
			this.logger.severe("Failed to update twitter status: " + e.getMessage());
			return "Failed to update twitter status";
		}
	}
}
