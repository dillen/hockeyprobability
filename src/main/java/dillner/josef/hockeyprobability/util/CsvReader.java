package dillner.josef.hockeyprobability.util;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.inject.Inject;

import com.opencsv.CSVReader;

public class CsvReader {

	@Inject
	private Logger logger; 
	
	public List<String[]> csvParser() {
		List<String[]> stats = new ArrayList<>();
		try {
			File file = new File(getClass().getResource("stats.csv").getFile());
			CSVReader csvReader = new CSVReader(new FileReader(file));
			stats = csvReader.readAll();
			csvReader.close();
			return stats;
		} catch (IOException e) {
			this.logger.severe("Failed parse csv: " + e.getMessage());
			return null;
		}
	}
}
