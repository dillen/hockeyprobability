package dillner.josef.hockeyprobability.util;

import java.io.File;
import java.util.Scanner;
import java.util.logging.Logger;

import javax.inject.Inject;

public class TwitterKeyReader {
	
	@Inject
	private Logger logger;

	public String getKey(String twitterKey) {
		String key = "";
		try {
			File file = new File(getClass().getResource(twitterKey+"_key.key").getFile());
			Scanner scanner = new Scanner(file);
			key = scanner.nextLine();
			scanner.close();
		} catch (Exception e) {
			this.logger.severe("Failed to get twitter key: " + e.getMessage());
		}
		return key;
	}
}
