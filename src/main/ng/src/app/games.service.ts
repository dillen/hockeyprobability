import { Injectable } from '@angular/core';
import { Headers,Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class GamesService {

    constructor(private http: Http) {}

    private headers = new Headers({'Content-Type': 'application/json'});
    private url = "http://localhost:8080/HockeyProbability/rest/games";

    getGames(): Promise<string[]> {
        return this.http.get(this.url, {headers: this.headers})
                        .toPromise()
                        .then(response => response.json() as string[]);
    }
}