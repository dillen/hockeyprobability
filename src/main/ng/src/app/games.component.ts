import  { Component, OnInit } from '@angular/core';

import { GamesService } from './games.service';

@Component({
    selector: 'games',
    templateUrl: './games.component.html',
    styleUrls: ['./games.component.css']
})

export class GamesComponent implements OnInit {

    games: string[];

    constructor(private gamesservice: GamesService) {}
    
    getGames():void {
        this.gamesservice.getGames().then(games => this.games = games);
    }
    ngOnInit(): void {
        this.getGames();
    }
}