import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {APP_BASE_HREF} from '@angular/common';

import { AppComponent } from './app.component';
import { GamesComponent } from './games.component';
import { GamesService } from './games.service';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    GamesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [GamesService, {provide: APP_BASE_HREF, useValue: environment.production ? "HockeyProbability/ng/" : "/"}],
  bootstrap: [AppComponent]
})
export class AppModule { }
